xacobeo (0.15-4) UNRELEASED; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Remove Jozef Kutej from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 30 Jan 2016 20:07:38 +0100

xacobeo (0.15-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Add debian/upstream/metadata
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Fri, 29 May 2015 17:31:53 +0200

xacobeo (0.15-2) unstable; urgency=medium

  * Team upload.
  * Strip trailing slash from metacpan URLs.
  * Add build-dependency on docbook-xml.
    Thanks to Niko Tyni for the analysis.
    (Closes: #750283)

 -- gregor herrmann <gregoa@debian.org>  Mon, 14 Jul 2014 16:40:32 +0200

xacobeo (0.15-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop patches, all merged upstream.
  * Build-depend on Module::Build 0.38.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Fri, 24 Jan 2014 20:16:00 +0100

xacobeo (0.13-3) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Use debhelper 9.20120312 to get all hardening flags.
  * Add patch to add Keywords entry to .desktop file.
  * Add patch to fix syntax errors with perl 5.18. (Closes: #720869)
  * Declare compliance with Debian Policy 3.9.4.

 -- gregor herrmann <gregoa@debian.org>  Mon, 26 Aug 2013 00:15:29 +0200

xacobeo (0.13-2) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * Pass the new multiarch directory to include_dirs in debian/rules,
    otherwise gdkconfig.h is not found, and build-depend on
    dpkg-dev (>= 1.16.0) (closes: #639060).
  * Set Standards-Version to 3.9.2 (no changes).
  * Bump debhelper compatibility level to 8.
  * debian/copyright: update license stanzas.

 -- gregor herrmann <gregoa@debian.org>  Wed, 24 Aug 2011 19:03:36 +0200

xacobeo (0.13-1) unstable; urgency=low

  * Initial Release. (Closes: #577200)

 -- Jozef Kutej <jozef@kutej.net>  Sat, 31 Jul 2010 15:18:33 +0200
